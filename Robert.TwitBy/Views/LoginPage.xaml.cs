﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Robert.TwitBy.ViewModels;

namespace Robert.TwitBy.Views
{
    public partial class LoginPage
    {
        public LoginPage(LoginViewModel model)
        {
            this.InitializeComponent();

            _model = model;

            this.DataContext=_model;
        }

        private LoginViewModel _model;
    }
}