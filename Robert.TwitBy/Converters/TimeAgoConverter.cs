﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

using TweetSharp.Extensions;

namespace Robert.TwitBy.Converters
{
    class TimeAgoConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var TimeAgo = DateTime.Parse(values[0] as string);
            var ID = values[1] as string;
            var textBlock = new TextBlock() { TextWrapping = TextWrapping.Wrap };

            textBlock.Inlines.Add(TimeAgo.ToRelativeTime());

            if (ID == "TwitBy" || ID == "Mention" || ID == "Direct" || ID == "Home" || ID == "Sent")
                textBlock.Foreground = new SolidColorBrush(Colors.White);
            else
                textBlock.Foreground = new SolidColorBrush(Colors.Black);

            return textBlock;
            
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
