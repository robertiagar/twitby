﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using System.Windows.Data;
using System.Windows.Media;
using Robert.TwitBy.Properties;

namespace Robert.TwitBy.Converters
{
    class TweetBackgroundConverter: IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var ID = value[2] as string;
            var IsMention = value[0] as bool?;
            var IsDirect = value[1] as bool?;
            var IsFavorite = value[3] as bool?;

            if (ID == "TwitBy" || ID == "RateLimit" || ID == "Mention" || ID == "Direct" || ID == "Home" || ID == "Sent")
                return App.Current.FindResource("TwitByBackground") as LinearGradientBrush;
            else
                if (IsMention.Value)
                    return App.Current.FindResource("MentionBackground") as LinearGradientBrush;
                else
                    if (IsDirect.Value)
                        return App.Current.FindResource("DirectBackground") as LinearGradientBrush;
                    else
                        if (IsFavorite.Value)
                            return App.Current.FindResource("FavoriteBackground") as LinearGradientBrush;
            //TODO: Implement some sort of a thing that knows which status is read and which not.

            /*if (string.Compare(s, Settings.Default.LastID) > 0)
                return Colors.White.ToString();
            else
                return Colors.LightGray.ToString();*/
            return App.Current.FindResource("DefaultBackground") as LinearGradientBrush;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
