﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Robert.TwitBy.Converters
{
    class UserMentionConverter : MarkupExtension, IValueConverter
    {
        private static Binding _mentionBinding = new Binding { ElementName = "me", Path = new PropertyPath("DataContext.MentionCommand") };

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var s = value as string;

            var link = new Hyperlink();
            link.Inlines.Add(s);
            link.SetBinding(Hyperlink.CommandProperty, _mentionBinding);
            link.Style = App.Current.FindResource("HyperlinkStyle") as Style;
            link.ToolTip = "Add " + s + " to your tweet message.";
            link.CommandParameter = s;

            if (s == "TwitBy")
                link.Foreground = new SolidColorBrush(Colors.White);
            else
                link.Foreground = new SolidColorBrush(Colors.Black);

            return link;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}