﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Timers;
using System.Diagnostics;
using System.Windows;

namespace Robert.TwitBy.Models
{
    public class Tweet : INotifyPropertyChanged, IEquatable<Tweet>
    {
        private string _status;
        private string _user;
        private string _profileImageURL;
        private DateTime _dateCreated;
        private string _id;
        private string _source;
        private bool _isMention = false;
        private bool _isDirect = false;
        private bool _isFavorite = false;
        private Timer _timer = new Timer() { Interval = 30000 };
        private bool TimerFlag = true;

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }
        public string User
        {
            get { return _user; }
            set
            {
                _user = value;
                OnPropertyChanged("User");
            }
        }
        public string ProfileImageURL
        {
            get { return _profileImageURL; }
            set
            {
                _profileImageURL = value;
                OnPropertyChanged("ProfileImageURL");
            }
        }
        public DateTime DateCreated
        {
            get { return _dateCreated; }
        }
        public string TimeAgo
        {
            get { return _dateCreated.ToString(); }
            set
            {
                _dateCreated = DateTime.Parse(value);
                OnPropertyChanged("TimeAgo");
            }
        }
        public string ID
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("ID");
            }
        }
        public string Source
        {
            get { return _source; }
            set
            {
                _source = value;
                OnPropertyChanged("Source");
            }
        }
        public bool IsMention
        {
            get { return _isMention; }
            set
            {
                _isMention = value;
                OnPropertyChanged("IsMention");
            }
        }
        public bool IsDirect
        {
            get { return _isDirect; }
            set
            {
                _isDirect = value;
                OnPropertyChanged("IsDirect");
            }
        }
        public bool IsFavorite
        {
            get { return _isFavorite; }
            set
            {
                _isFavorite = value;
                OnPropertyChanged("IsFavorite");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (TimerFlag)
            {
                _timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
                _timer.Start();
                TimerFlag = false;
            }
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        //Every 30 seconds make WPF think that the Tweets' TimeAgo Property is changed...
        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            OnPropertyChanged("TimeAgo");
        }
        #endregion

        public bool Equals(Tweet other)
        {
            if (other.ID == this.ID)
                return true;
            else
                return false;
        }

        public static bool Equals(Tweet one, Tweet two)
        {
            if(one.ID == two.ID)
                return true;
            else
                return false;
        }
    }
}
