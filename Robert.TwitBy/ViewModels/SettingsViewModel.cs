﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robert.TwitBy.Properties;
using System.Windows.Threading;
using System.Windows.Input;

namespace Robert.TwitBy.ViewModels
{
    public class SettingsViewModel: MainViewModel
    {
        public SettingsViewModel(Dispatcher dispatcher, MainViewModel model) :
            base(dispatcher,model)
        {
            _dipatcher = dispatcher;
            _model = model;
        }

        private Dispatcher _dipatcher;
        private MainViewModel _model;
        private ICommand _goBack;

        public ICommand GoBackCommand
        {
            get
            {
                if (_goBack == null)
                    _goBack = new RelayCommand(param => this.GoBack());
                return _goBack;
            }
        }

        public double RefreshInterval
        {
            get { return double.Parse(Settings.Default.RefreshInterval.Minutes.ToString()); }
            set
            {
                Settings.Default.RefreshInterval = new TimeSpan(0, int.Parse(value.ToString()), 0);
                OnPropertyChanged("RefreshInterval");
            }
        }

        private void GoBack()
        {
            _model.NavigationService.GoBack();
        }
    }
}
