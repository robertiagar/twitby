﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Threading;

using Robert.TwitBy.Views;

namespace Robert.TwitBy.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel(Dispatcher dispatcher)
        {
            Dispatcher = dispatcher;
        }

        public MainViewModel(Dispatcher dispatcher, MainViewModel model)
        {
            Dispatcher = dispatcher;
        }

        private string _title;

        public Dispatcher Dispatcher{get;set;}

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        public NavigationService NavigationService { get; set; }

        public virtual event PropertyChangedEventHandler PropertyChanged;

        public void GoToLogin(LoginViewModel model)
        {
            NavigationService.Navigate(new LoginPage(model));
            Title = "Login";
        }

        public void GoToTweets(TwitterViewModel model)
        {
            NavigationService.Navigate(new MainPage(model));
            Title = "Tweets";
        }

        public void GoToSettings(SettingsViewModel model)
        {
            NavigationService.Navigate(new SettingsPage(model));
            Title = "Settings";
        }

        public virtual void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public double MaxHeight
        {
            get
            {
                return System.Windows.SystemParameters.PrimaryScreenHeight - 50;
            }
        }
    }
}
