﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Robert.TwitBy.ViewModels;
using Robert.TwitBy.Properties;
using Robert.TwitBy.Views;

namespace Robert.TwitBy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel _model;
        private TwitterViewModel _tmodel;
        private LoginViewModel _lmodel;

        public MainWindow(MainViewModel model, TwitterViewModel tmodel, LoginViewModel lmodel)
        {
            this.InitializeComponent();

            this.Width = Settings.Default.WindowWidth;
            this.Height = Settings.Default.WindowHeight;
            this.Left = Settings.Default.WindowLeft;
            this.Top = Settings.Default.WindowTop;
            this.DataContext = model;
            this.mainFrame.Navigated += new System.Windows.Navigation.NavigatedEventHandler(mainFrame_Navigated);

            _model = model;
            model.NavigationService = mainFrame.NavigationService;

            _tmodel = tmodel;
            tmodel.Dispatcher = this.Dispatcher;
            tmodel.NavigationService = this.mainFrame.NavigationService;

            _lmodel = lmodel;
            lmodel.Dispatcher = this.Dispatcher;
            lmodel.NavigationService = this.mainFrame.NavigationService;

            //If any of these are null, app will crash.
            if (string.IsNullOrEmpty(Settings.Default.AccessToken) ||
                string.IsNullOrEmpty(Settings.Default.UserId) ||
                string.IsNullOrEmpty(Settings.Default.AccessTokenSecret) ||
                Settings.Default.Followers == null)
            {
                model.GoToLogin(lmodel);
            }
            else
            {
                model.GoToTweets(tmodel);
            }
        }

        void mainFrame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            mainFrame.NavigationService.RemoveBackEntry();

            GC.Collect();

            if (e.Content is LoginPage)
            {
                this.InputBindings.Clear();
                foreach (var ib in _lmodel.InputBindings)
                    this.InputBindings.Add(ib);
                return;
            }
            if (e.Content is MainPage)
            {
                this.InputBindings.Clear();
                foreach (var ib in _tmodel.InputBindings)
                    this.InputBindings.Add(ib);
                return;
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (MessageBox.Show("Close TwitBy?", "Close", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
            {
                e.Cancel = true;
                return;
            }

            Settings.Default.WindowTop = RestoreBounds.Top;
            Settings.Default.WindowLeft = RestoreBounds.Left;
            Settings.Default.WindowHeight = RestoreBounds.Height;
            Settings.Default.WindowWidth = RestoreBounds.Width;
            Settings.Default.Save();
        }
    }
}
