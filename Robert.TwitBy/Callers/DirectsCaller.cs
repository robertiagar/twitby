﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robert.TwitBy.Models;
using Robert.TwitBy.Properties;
using Robert.TwitBy.ViewModels;

using TweetSharp.Twitter.Extensions;
using TweetSharp.Twitter.Model;

namespace Robert.TwitBy.Callers
{
    class DirectsCaller:TwitterCaller
    {

        public DirectsCaller(TwitterViewModel model) :
            base(model)
        {
            _model = model;
        }

        private TwitterViewModel _model;
        private Tweet _gettingDirectTweets;

        private Tweet GettingDirectTweets
        {
            get
            {
                if (_gettingDirectTweets == null)
                    _gettingDirectTweets = new Tweet
                    {
                        User = "TwitBy",
                        Source = "TwitBy",
                        ProfileImageURL = "images/twitby.png",
                        ID = "Direct",
                        TimeAgo = DateTime.Now.ToString(),
                        Status = "Getting your Directs Timeline tweets. Bare with me this might take a while."
                    };
                return _gettingDirectTweets;
            }
        }


        protected override void OnSuccesfulResponse(TwitterResult result)
        {
            var tweets = result.AsDirectMessages();
            _model.RateLimitStatus = result.RateLimitStatus.RemainingHits + "/" + result.RateLimitStatus.HourlyLimit;
            _model.Dispatcher.BeginInvoke(new Action(() =>
            {
                Parallel.ForEach((from dms in tweets.AsParallel()
                                  select dms), new Action<TwitterDirectMessage>(dm =>
                                  {
                                      _model.Dispatcher.BeginInvoke(new Action(() =>
                                      {
                                          var tweet = new Tweet();

                                          tweet.User = dm.Sender.ScreenName;
                                          tweet.Source = null;
                                          tweet.Status = dm.Text;
                                          tweet.ID = dm.Id.ToString();
                                          tweet.ProfileImageURL = dm.Sender.ProfileImageUrl;
                                          tweet.TimeAgo = dm.CreatedDate.ToLocalTime().ToString();
                                          tweet.IsDirect = true;

                                          if (!_model.Tweets.Contains<Tweet>(tweet))
                                              _model.Tweets.Add(tweet);
                                      }));
                                  }));
                /*var list = (from dms in tweets
                            select new Tweet
                            {
                                User = dms.Sender.ScreenName,
                                Source = null,
                                Status = dms.Text,
                                ID = dms.Id.ToString(),
                                ProfileImageURL = dms.Sender.ProfileImageUrl,
                                TimeAgo = dms.CreatedDate.ToLocalTime().ToString()
                            }).Union(_model.Tweets).AsParallel();
                foreach (var sm in list)
                    _model.Tweets.Add(sm);*/
                GotTweets();
            }));
        }

        protected override void GettingTweets()
        {
            _model.Tweets.Add(GettingDirectTweets);
        }

        protected override void GotTweets()
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingDirectTweets);
            _gettingDirectTweets = null;
        }

        protected override void IsTwitterError(TwitterResult result)
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingDirectTweets);
            _gettingDirectTweets = null;
            _model.RefreshTimelineCommand.Execute(null);
        }

        protected override void IsTwitterDown(TwitterResult result)
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingDirectTweets);
            _gettingDirectTweets = null;
            _model.RefreshTimelineCommand.Execute(null);
        }
    }
}
