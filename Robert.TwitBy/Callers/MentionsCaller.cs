﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robert.TwitBy.Models;
using Robert.TwitBy.Properties;
using Robert.TwitBy.ViewModels;

using TweetSharp.Twitter.Extensions;
using TweetSharp.Twitter.Model;

namespace Robert.TwitBy.Callers
{
    class MentionsCaller:TwitterCaller
    {
        public MentionsCaller(TwitterViewModel model) :
            base(model)
        {
            _model = model;
        }

        private TwitterViewModel _model;
        private Tweet _gettingMentionTweets;

        private Tweet GettingMentionTweets
        {
            get
            {
                if (_gettingMentionTweets == null)
                    _gettingMentionTweets = new Tweet
                    {
                        User = "TwitBy",
                        Source = "TwitBy",
                        ProfileImageURL = "images/twitby.png",
                        ID = "Mention",
                        TimeAgo = DateTime.Now.ToString(),
                        Status = "Getting your Mentions Timeline tweets. Bare with me this might take a while."
                    };
                return _gettingMentionTweets;
            }
        }

        protected override void OnSuccesfulResponse(TwitterResult result)
        {
            var tweets = result.AsStatuses();
            _model.RateLimitStatus = result.RateLimitStatus.RemainingHits + "/" + result.RateLimitStatus.HourlyLimit;
            Parallel.ForEach((from sms in tweets.AsParallel()
                              select sms), new Action<TwitterStatus>(sm =>
                              {
                                  _model.Dispatcher.BeginInvoke(new Action(() =>
                                      {
                                          var tweet = new Tweet();

                                          tweet.User = sm.User.ScreenName;
                                          tweet.Source = sm.Source;
                                          tweet.Status = sm.Text;
                                          tweet.ID = sm.Id.ToString();
                                          tweet.ProfileImageURL = sm.User.ProfileImageUrl;
                                          tweet.TimeAgo = sm.CreatedDate.ToLocalTime().ToString();
                                          tweet.IsMention = true;
                                          tweet.IsFavorite = sm.IsFavorited;

                                          if (!_model.Tweets.Contains<Tweet>(tweet))
                                              _model.Tweets.Add(tweet);
                                      }));
                              }));
            _model.Dispatcher.BeginInvoke(new Action(() => GotTweets()));
        }

        protected override void GettingTweets()
        {
            _model.Tweets.Add(GettingMentionTweets);
        }

        protected override void GotTweets()
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingMentionTweets);
            _gettingMentionTweets = null;
        }

        protected override void IsTwitterError(TwitterResult result)
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingMentionTweets);
            _gettingMentionTweets = null;
            _model.RefreshTimelineCommand.Execute(null);
        }

        protected override void IsTwitterDown(TwitterResult result)
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingMentionTweets);
            _gettingMentionTweets = null;
            _model.RefreshTimelineCommand.Execute(null);
        }
    }
}
