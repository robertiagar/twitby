﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robert.TwitBy.Models;
using Robert.TwitBy.Properties;
using Robert.TwitBy.ViewModels;

using TweetSharp.Twitter.Extensions;
using TweetSharp.Twitter.Model;

namespace Robert.TwitBy.Callers
{
    class YourCaller:TwitterCaller
    {
        public YourCaller(TwitterViewModel model):
            base(model)
        {
            _model = model;
        }

        private TwitterViewModel _model;
        private Tweet _gettingYourTweets;

        private Tweet GettingYourTweets
        {
            get
            {
                if (_gettingYourTweets == null)
                    _gettingYourTweets = new Tweet
                    {
                        User = "TwitBy",
                        Source = "TwitBy",
                        ProfileImageURL = "images/twitby.png",
                        ID = "Sent",
                        TimeAgo = DateTime.Now.ToString(),
                        Status = "Getting the tweets you sent. Bare with me this might take a while."
                    };
                return _gettingYourTweets;
            }
        }

        protected override void OnSuccesfulResponse(TwitterResult result)
        {
            var tweets = result.AsStatuses();
            _model.RateLimitStatus = result.RateLimitStatus.RemainingHits + "/" + result.RateLimitStatus.HourlyLimit;
            _model.Dispatcher.BeginInvoke(new Action(() =>
            {
                Parallel.ForEach((from sms in tweets.AsParallel()
                                  select sms), new Action<TwitterStatus>(sm =>
                                  {
                                      _model.Dispatcher.BeginInvoke(new Action(() =>
                                          {
                                              var tweet = new Tweet();

                                              tweet.User = sm.User.ScreenName;
                                              tweet.Source = sm.Source;
                                              tweet.Status = sm.Text;
                                              tweet.ID = sm.Id.ToString();
                                              tweet.ProfileImageURL = sm.User.ProfileImageUrl;
                                              tweet.TimeAgo = sm.CreatedDate.ToLocalTime().ToString();
                                              tweet.IsFavorite = sm.IsFavorited;
                                              tweet.IsMention = sm.Text.ToLower().Contains(string.Format("@{0}", Settings.Default.UserId.ToLower()));

                                              if (!_model.Tweets.Contains<Tweet>(tweet))
                                                  _model.Tweets.Add(tweet);
                                          }));
                                  }));
                /*var list = (from sms in tweets
                            select new Tweet
                            {
                                User = sms.User.ScreenName,
                                Source = sms.Source,
                                Status = sms.Text,
                                ID = sms.Id.ToString(),
                                ProfileImageURL = sms.User.ProfileImageUrl,
                                TimeAgo = sms.CreatedDate.ToLocalTime().ToString()
                            }).Union(_model.Tweets).AsParallel();
                foreach (var sm in list)
                    _model.Tweets.Add(sm);*/
                GotTweets();
            }));
        }

        protected override void GettingTweets()
        {
            _model.Tweets.Add(GettingYourTweets);
        }

        protected override void GotTweets()
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingYourTweets);
            _gettingYourTweets = null;
        }

        protected override void IsTwitterError(TwitterResult result)
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingYourTweets);
            _gettingYourTweets = null;
            _model.RefreshTimelineCommand.Execute(null);
        }

        protected override void IsTwitterDown(TwitterResult result)
        {
            _model.IsRefreshing = false;
            _model.Tweets.Remove(GettingYourTweets);
            _gettingYourTweets = null;
            _model.RefreshTimelineCommand.Execute(null);
        }
    }
}
