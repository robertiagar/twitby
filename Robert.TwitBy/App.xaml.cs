﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Windows;
using Robert.TwitBy.ViewModels;
using Robert.TwitBy.Properties;

namespace Robert.TwitBy
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //fresh start
            //Settings.Default.AccessToken = null;
            //Settings.Default.AccessTokenSecret = null;
            MainViewModel model = new MainViewModel(this.Dispatcher);
            TwitterViewModel tmodel = new TwitterViewModel(this.Dispatcher, model);
            LoginViewModel lmodel = new LoginViewModel(this.Dispatcher,model,tmodel);

            new MainWindow(model, tmodel, lmodel).ShowDialog();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            Settings.Default.Save();
        }
	}
}